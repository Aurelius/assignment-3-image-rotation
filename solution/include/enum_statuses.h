enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_EMPTY,
    READ_FAIL,
    READ_WITH_ERROR_FSEEK,
    READ_INVALID_COUNT_OFF_BITS
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_PIXELS_ERROR,
    WRITE_PADDINGS_ERROR
};

enum util_status {
    INVALID_ARGUMENTS_COUNT,
    FILE_OPEN_ERROR,
    FILE_CREATION_ERROR
};
