#include "bmp_utils.h"
#include <stdio.h>

enum read_status read_new_header(FILE *in, struct bmp_header *header);

enum read_status read_pixels(FILE *in, struct image const *img);
