#ifndef IMAGE
#define IMAGE

#include "stdlib.h"
#include <inttypes.h>
#include <stdbool.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};
#endif

bool create_image(struct image *image, const uint64_t width, const uint64_t height);

void image_destroy(struct image *image);
