#ifndef BMP_UTILS
#define BMP_UTILS
#include "image.h"
#include "output.h"
#include "rotate.h"
#include "utils.h"
#include <stdint.h>
#include <stdio.h>

enum params {
    BF_TYPE = 0x4D42,
    BF_RESERVED = 0,
    BI_SIZE = 40,
    BI_PLANES = 1,
    BI_COUNT = 24,
    BI_COMPRESSION = 0,
    PIXELS_PER_METER = 2834,
    BI_CLR_USED = 0,
    BI_CLR_IMPORTANT = 0
};

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

bool rotate_bmp(FILE *in, FILE *out);
#endif
