#include "bmp_utils.h"

static const char* UTIL_OUTPUT_ENUM_TO_TEXT[] = {
        [INVALID_ARGUMENTS_COUNT] = "Error! Invalid arguments count\n",
        [FILE_OPEN_ERROR] = "Error on opening file\n",
        [FILE_CREATION_ERROR] = "Error on file creation\n "
};

int main(int argc, char **argv) {
    if (argc != 3) {
        std_output_error(UTIL_OUTPUT_ENUM_TO_TEXT[INVALID_ARGUMENTS_COUNT]);
    }

    FILE *in = NULL;
    FILE *out = NULL;

    in = fopen(argv[1], "rb");
    out = fopen(argv[2], "wb");

    if (in == NULL) {
        std_output_error(UTIL_OUTPUT_ENUM_TO_TEXT[FILE_OPEN_ERROR]);
    } else if (out == NULL) {
        std_output_error(UTIL_OUTPUT_ENUM_TO_TEXT[FILE_CREATION_ERROR]);
    }

    rotate_bmp(in, out);

    fclose(in);
    fclose(out);
    return 0;
}
