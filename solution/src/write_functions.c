#include "write_functions.h"

enum write_status write_pixels(FILE *out, struct image const *img) {
    size_t padding = get_image_padding(img->width);
    uint32_t paddings[3] = {0}; // 3 - max value of paddings.

    for (size_t line = 0; line < img->height; line++) {
        size_t dataResult = fwrite((img->width) * line + img->data, img->width * sizeof(struct pixel), 1, out);
        if (fwrite(paddings, padding, 1, out) != 1) {
            return WRITE_PADDINGS_ERROR;
        }

        if (dataResult != 1) {
            return WRITE_PIXELS_ERROR;
        }
    }

    return WRITE_OK;
}
