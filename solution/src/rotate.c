#include "image.h"
#include "rotate.h"

struct image rotate(const struct image *img) {
    struct image out;

    uint64_t width = img->width, height = img->height;
    create_image(&out, height, width);
    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            out.data[(height) * (j + 1) - i - 1] = img->data[(i * width) + j];
        }
    }

    return out;
}
