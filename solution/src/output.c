#include "output.h"

void std_output(const char* s) {
    fprintf(stdout, "%s", s);
}

void std_output_error(const char* s) {
    fprintf(stderr, "%s", s);
    exit(1);
}
