#include "read_functions.h"
#include "write_functions.h"

static struct bmp_header create_bmp_header(const struct image *image) {
    struct bmp_header header = {0};
    size_t padding = get_image_padding(image->width);

    header.bfType = BF_TYPE;
    header.bfileSize = header.bOffBits + header.biSizeImage;
    header.bfReserved = BF_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = image->height * (image->width * sizeof(struct pixel) + padding);
    header.biXPelsPerMeter = PIXELS_PER_METER;
    header.biYPelsPerMeter = PIXELS_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;

    return header;
}

static const char* READ_OUTPUT_ENUM_TO_TEXT[] = {
        [READ_OK] = "Content successfully read\n",
        [READ_INVALID_HEADER] = "Header reading error\n",
        [READ_EMPTY] = "File or image not found\n",
        [READ_FAIL] = "File not valid for reading\n",
        [READ_WITH_ERROR_FSEEK] = "Can't read file because moving stream pointer is unreachable\n",
        [READ_INVALID_COUNT_OFF_BITS] = "Read count of bits not equal to image bits count\n"
};

static const char* WRITE_OUTPUT_ENUM_TO_TEXT[] = {
        [WRITE_OK] = "Data recording completed\n",
        [WRITE_ERROR] = "Image recording error\n",
        [WRITE_PIXELS_ERROR] = "Can't write pixels\n",
        [WRITE_PADDINGS_ERROR] = "Can't write paddings\n"
};

enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in || !img) {
        return READ_EMPTY;
    }

    struct bmp_header header = {0};
    if (read_new_header(in, &header)) {
        return READ_INVALID_HEADER;
    }

    if (create_image(img, header.biWidth, header.biHeight)) {
        return READ_FAIL;
    }

    enum read_status read_pixels_status = read_pixels(in, img);
    if (read_pixels_status) {
        return read_pixels_status;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = create_bmp_header(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    enum write_status write_pixels_status = write_pixels(out, img);
    if (write_pixels_status) {
        return write_pixels_status;
    }

    return WRITE_OK;
}

bool rotate_bmp(FILE *in, FILE *out) {
    struct image original_bmp;
    struct image rotated_bmp;

    enum read_status readStatus = from_bmp(in, &original_bmp);
    if (readStatus) {
        std_output_error(READ_OUTPUT_ENUM_TO_TEXT[readStatus]);
    }

    std_output(READ_OUTPUT_ENUM_TO_TEXT[readStatus]); // if read success

    rotated_bmp = rotate(&original_bmp);

    enum write_status writeStatus = to_bmp(out, &rotated_bmp);
    if (writeStatus) {
        std_output_error(WRITE_OUTPUT_ENUM_TO_TEXT[writeStatus]);
    }

    std_output(WRITE_OUTPUT_ENUM_TO_TEXT[writeStatus]); // if write success

    image_destroy(&original_bmp);
    image_destroy(&rotated_bmp);

    return 0;
}
