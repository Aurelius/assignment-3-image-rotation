#include <read_functions.h>

enum read_status read_new_header(FILE *in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum read_status read_pixels(FILE *in, struct image const *img) {
    size_t padding = get_image_padding(img->width);

    for (size_t line = 0; line < img->height; line++) {
        size_t dataResult = fread((img->width) * line + img->data, sizeof(struct pixel), img->width, in);
        if (dataResult != img->width) {
            return READ_INVALID_COUNT_OFF_BITS;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            return READ_WITH_ERROR_FSEEK;
        }
    }

    return READ_OK;
}
