#include "image.h"

bool create_image(struct image *image, const uint64_t width, const uint64_t height) {
    image->height = height;
    image->width = width;
    image->data = malloc(sizeof(struct pixel) * height * width);

    if (image->data == NULL) {  // if not allocated.
        return true;
    } else { return false; }
}

void image_destroy(struct image *image) {
    free(image->data);
}
