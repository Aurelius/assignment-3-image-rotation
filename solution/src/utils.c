#include "image.h"
#include "utils.h"

uint64_t get_image_padding(const uint64_t width) {
    uint64_t memory_padding = 4 - (width * sizeof(struct pixel)) % 4;
    return memory_padding % 4;
}
